package ru.examples.my.ImageRedactor;

import android.app.Application;

import ru.examples.my.ImageRedactor.DI.AppCompanent;
import ru.examples.my.ImageRedactor.DI.AppModule;
import ru.examples.my.ImageRedactor.DI.DaggerAppCompanent;

public class App extends Application {


    private static AppCompanent companent;

    public static AppCompanent getCompanent() {
        return companent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        companent = buildComponent();
    }


    protected AppCompanent buildComponent()
    {  return DaggerAppCompanent
            .builder()
            .appModule(new AppModule(this))
            .build();
    }
}
