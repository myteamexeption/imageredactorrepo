package ru.examples.my.ImageRedactor.Data;

import android.content.Context;

import io.reactivex.Observable;
import ru.examples.my.ImageRedactor.Domain.IRepozitory;
import ru.examples.my.ImageRedactor.Domain.ImageData;

public class ImageRepozitory implements IRepozitory {



    private Context context;
    public ImageRepozitory(Context context)
    {
        this.context  = context;
    }

    @Override
    public Observable<ImageData> getImagesInMemomry() {

        Observable<ImageData> imageDataObservable = Observable.fromArray(new ImageData());

        return imageDataObservable;


    }

    @Override
    public Observable<ImageData> getImageInUrl() {
        Observable<ImageData> imageDataObservable = Observable.fromArray(new ImageData());
        return imageDataObservable;
    }
}
