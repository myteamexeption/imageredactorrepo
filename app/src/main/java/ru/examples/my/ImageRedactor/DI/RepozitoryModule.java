package ru.examples.my.ImageRedactor.DI;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;
import ru.examples.my.ImageRedactor.Data.ImageRepozitory;
import ru.examples.my.ImageRedactor.Domain.IRepozitory;

@Module
public class RepozitoryModule
{

        @NonNull
        @Provides
        @Singleton
        public IRepozitory providerRepozitory(Context context)
        {
            return new ImageRepozitory(context);
        }


}
