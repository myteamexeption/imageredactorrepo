package ru.examples.my.ImageRedactor.Domain;


import io.reactivex.Observable;

public interface IRepozitory
{


    public Observable<ImageData> getImagesInMemomry();

    public Observable<ImageData> getImageInUrl();


}
