package ru.examples.my.ImageRedactor.DI;


import javax.inject.Singleton;

import dagger.Component;
import ru.examples.my.ImageRedactor.Presentation.MainActivity;

@Component(modules = {AppModule.class ,RepozitoryModule.class})
@Singleton
public interface AppCompanent {

    void inject(MainActivity mainActivity);

}
