package ru.examples.my.ImageRedactor.Presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



import javax.inject.Inject;

import io.reactivex.Observable;
import ru.examples.my.ImageRedactor.App;
import ru.examples.my.ImageRedactor.Domain.IRepozitory;
import ru.examples.my.ImageRedactor.Domain.ImageData;
import ru.examples.my.ImageRedactor.R;

public class MainActivity extends AppCompatActivity implements IViewMain {

    @Inject
    IRepozitory repozitory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.getCompanent().inject(this);

        Observable<ImageData> imageDataObservable = repozitory.getImageInUrl();


    }


}
